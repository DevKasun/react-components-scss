import React, { Component } from 'react';
import './App.scss';
import Header from './components/Header';
import Cover from './components/Cover';
import Footer from './components/Footer';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header></Header>
        <Cover></Cover>
        <Footer></Footer>
      </div>
    );
  }
}

export default App;
